// swift-tools-version:5.1
import PackageDescription

let package = Package(
    name: "xic",
    products: [
        .library(
            name: "xic",
            targets: ["xic"])
    ],

    targets: [
        .systemLibrary(
            name: "Clibxml2",
            pkgConfig: "libxml-2.0",
            providers: [
                .brew(["libxml2"]),
                .apt(["libxml2-dev"])
            ]),
        .target(
            name: "xic",
            dependencies: ["Clibxml2"]),
        .testTarget(
            name: "xicTests",
            dependencies: ["xic"])
    ]
)
