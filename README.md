# xic /[ʃik][]/

Simple [libxml2][] bindings for Swift inspired by [Kanna][].

[libxml2]:  http://xmlsoft.org
[Kanna]: https://github.com/tid-kijyun/Kanna
[ʃik]: https://en.wiktionary.org/wiki/xic#Catalan
