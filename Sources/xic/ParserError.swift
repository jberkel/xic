import Foundation

public enum ParserError: LocalizedError {
    case encoding
    case invalidXml(line: Int, message: String)
    case unknown

    public var errorDescription: String? {
        switch self {
        case .encoding:
            return "encoding error"
        case .invalidXml(let line, let message):
            return "line \(line): \(message)"
        case .unknown:
            return "unknown"
        }
    }
}
