import Foundation

extension String {
    var trim: String {
        trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
