import Foundation
import Clibxml2

public typealias XMLNodeSet = [XmlElement]

extension Array where Element: XmlElement {
    var description: String {
        reduce("") { $0 + $1.description }
    }
}

public enum XPathObject {
    case nodeSet(XMLNodeSet)
    case bool(Bool)
    case number(Double)
    case string(String)
}

extension XPathObject {
    init?(docPtr: UnsafePointer<xmlDoc>, object: xmlXPathObject) {
        switch object.type {
        case XPATH_NODESET:
            guard let nodeSet = object.nodesetval,
                  nodeSet.pointee.nodeNr > 0,
                  nodeSet.pointee.nodeTab != nil else {
                return nil
            }
            var nodes: [XmlElement] = []
            let size = Int(nodeSet.pointee.nodeNr)
            for i in 0..<size {
                let node: xmlNodePtr = nodeSet.pointee.nodeTab[i]!
                let element = XmlElement(node: node)
                nodes.append(element)
            }
            self = .nodeSet(XMLNodeSet(nodes))
        case XPATH_BOOLEAN:
            self = .bool(object.boolval != 0)
        case XPATH_NUMBER:
            self = .number(object.floatval)
        case XPATH_STRING:
            guard let result = object.stringval else { return nil }
            self = .string(String(cString: result))
        default:
            return nil
        }
    }
}

public extension Xic {
    // swiftlint:disable:next identifier_name
    static let XHTML_NS = ["xhtml": "http://www.w3.org/1999/xhtml"]

    func first(_ xpath: String, namespaces: [String: String]? = nil) -> XmlElement? {
        guard case .nodeSet(let nodes) = self.xpath(xpath, namespaces: namespaces) else { return nil }
        return nodes.first
    }

    /// - Parameters:
    ///   - xpath: the xpath expression
    ///   - namespaces: namespace prefixes cannot be NULL or empty string
    func xpath(_ xpath: String, namespaces: [String: String]? = nil) -> XPathObject? {
        self.xpath(xpath, contextNode: root, namespaces: namespaces)
    }

    func xpath(_ xpath: String, contextNode: XmlElement, namespaces: [String: String]? = nil) -> XPathObject? {
        guard let context = xmlXPathNewContext(docPointer) else {
            return nil
        }
        defer { xmlXPathFreeContext(context) }
        context.pointee.node = contextNode.nodePointer

        if let namespaces = namespaces {
            for (namespace, name) in namespaces {
                xmlXPathRegisterNs(context, namespace, name)
            }
        }

        guard let result = xmlXPathEvalExpression(xpath, context) else {
            return nil
        }
        defer {
            xmlXPathFreeObject(result)
        }
        return XPathObject(docPtr: docPointer, object: result.pointee)
    }
}
