import Foundation
import Clibxml2

public class Xic: CustomStringConvertible, CustomDebugStringConvertible, Sequence {
    public typealias Iterator = AnyIterator<XmlElement>
    public let root: XmlElement
    let docPointer: UnsafeMutablePointer<xmlDoc>

    public init(root: XmlElement) {
        docPointer = xmlNewDoc(nil)
        xmlDocSetRootElement(docPointer, root.nodePointer)
        self.root = root
    }

    public init(string: String, options: ParseOptions = [.noError]) throws {
        guard let doc = xmlReadDoc(string, "", "utf-8", Int32(options.rawValue)) else {
            if let xmlError: xmlErrorPtr = xmlGetLastError() {
                let message = String(cString: xmlError.pointee.message)
                throw ParserError.invalidXml(line: Int(xmlError.pointee.line), message: message.trim)
            } else {
                throw ParserError.unknown
            }
        }
        self.docPointer = doc
        self.root = XmlElement(node: xmlDocGetRootElement(docPointer))
    }

    public func makeIterator() -> Iterator {
        AnyIterator(XmlDescendantsAndSiblingsIterator(start: root))
    }

    deinit {
        xmlFreeDoc(docPointer)
    }

    public var description: String {
        var buffer: UnsafeMutablePointer<xmlChar>?
        xmlDocDumpMemory(docPointer, &buffer, nil)
        guard let buf = buffer else {
            return ""
        }
        defer { xmlFree(buffer) }
        let xml = String(cString: UnsafePointer<UInt8>(buf))
        if xml.starts(with: "<?xml"), let index = xml.firstIndex(of: ">") {
            return xml.suffix(from: xml.index(after: index)).description.trim
        } else {
            return xml
        }
    }

    public var prettyPrint: String {
        guard let buffer = xmlBufferCreate() else {
            return ""
        }
        defer { xmlBufferFree(buffer) }
        guard xmlNodeDump(buffer, docPointer, root.nodePointer, Int32(0), 1) >= 0 else { return "" }
        return String(cString: buffer.pointee.content)
    }

    public var debugDescription: String {
        self.map { $0.debugDescription }.joined(separator: "\n")
    }

    public struct ParseOptions: OptionSet {
        public let rawValue: UInt32

        public init(rawValue: UInt32) {
            self.rawValue = rawValue
        }

        init(_ option: xmlParserOption) {
            self.init(rawValue: option.rawValue)
        }

        // recover on errors
        public static let recover = ParseOptions(XML_PARSE_RECOVER)

        // substitute entities
        public static let noEntities = ParseOptions(XML_PARSE_NOENT)
        // remove blank nodes
        public static let noBlanks = ParseOptions(XML_PARSE_NOBLANKS)
        // suppress error reports
        public static let noError = ParseOptions(XML_PARSE_NOERROR)
        // suppress warning reports
        public static let noWarning = ParseOptions(XML_PARSE_NOWARNING)
        // pedantic error reporting
        public static let pedantic = ParseOptions(XML_PARSE_PEDANTIC)
        // forbid network access
        public static let noNetwork = ParseOptions(XML_PARSE_NONET)
        // remove redundant namespaces declarations
        public static let nsClean = ParseOptions(XML_PARSE_NSCLEAN)
        // merge CDATA as text nodes
        public static let noCDATA = ParseOptions(XML_PARSE_NOCDATA)
        // load the external subset
        public static let dtdLoad = ParseOptions(XML_PARSE_DTDLOAD)
        // default DTD attributes
        public static let dtdAttr = ParseOptions(XML_PARSE_DTDATTR)
        // validate with the DTD
        public static let dtdValid = ParseOptions(XML_PARSE_DTDVALID)

        // use the SAX1 interface internally
        public static let sax1 = ParseOptions(XML_PARSE_SAX1)
        // use the SAX1 interface internally
        public static let xinclude = ParseOptions(XML_PARSE_XINCLUDE)
        // do not reuse the context dictionary
        public static let nodict = ParseOptions(XML_PARSE_NODICT)
        // do not generate XINCLUDE START/END nodes
        public static let noXincNode = ParseOptions(XML_PARSE_NOXINCNODE)
        // compact small text nodes; no modification of the tree allowed afterwards
        public static let compact = ParseOptions(XML_PARSE_COMPACT)
        // parse using XML-1.0 before update 5
        public static let old10 = ParseOptions(XML_PARSE_OLD10)
        // do not fixup XINCLUDE xml:base uris
        public static let nobaseix = ParseOptions(XML_PARSE_NOBASEFIX)
        // relax any hardcoded limit from the parser
        public static let huge = ParseOptions(XML_PARSE_HUGE)
        // parse using SAX2 interface before 2.7.0
        public static let oldSax = ParseOptions(XML_PARSE_OLDSAX)
        // ignore internal document encoding hint
        public static let ignoreEnc = ParseOptions(XML_PARSE_IGNORE_ENC)
        // store big lines numbers in text PSVI field
        public static let bigLines = ParseOptions(XML_PARSE_BIG_LINES)
    }
}
