import Foundation
import Clibxml2

public class XmlElement: Sequence, CustomStringConvertible, CustomDebugStringConvertible, Equatable, Hashable {
    public typealias Iterator = AnyIterator<XmlElement>
    var nodePointer: UnsafeMutablePointer<xmlNode>

    init(node: xmlNodePtr) {
        self.nodePointer = node
    }

    public init(name: String, attributes: [String: String]? = nil) {
        guard let nodePointer = xmlNewNode(nil, name) else {
            preconditionFailure("Could not create node")
        }
        self.nodePointer = nodePointer
        if let attributes = attributes {
            for key in attributes.keys.sorted() {
                self[key] = attributes[key]
            }
        }
    }

    public init(text: String) {
        guard let nodePointer = xmlNewText(text) else {
            preconditionFailure("Could not create text node")
        }
        self.nodePointer = nodePointer
    }

    public func makeIterator() -> Iterator {
        descendants
    }

    public subscript(attributeName: String) -> String? {
        get {
            guard let result = xmlGetProp(nodePointer, attributeName) else { return nil }
            return String(cString: result)
        }
        set(newValue) {
            if let newValue = newValue {
                xmlSetProp(nodePointer, attributeName, newValue)
            } else {
                xmlUnsetProp(nodePointer, attributeName)
            }
        }
    }

    public var parent: XmlElement? {
        nodePointer.pointee.parent.map(XmlElement.init)
    }

    public var next: XmlElement? {
        nodePointer.pointee.next.map(XmlElement.init)
    }

    public var previous: XmlElement? {
        nodePointer.pointee.prev.map(XmlElement.init)
    }

    public var children: XmlElement? {
        nodePointer.pointee.children.map(XmlElement.init)
    }

    public var type: XmlElementType? {
        XmlElementType(rawValue: nodePointer.pointee.type.rawValue)
    }

    public func remove(child: XmlElement) {
        xmlUnlinkNode(child.nodePointer)
        xmlFreeNode(child.nodePointer)
    }

    @discardableResult
    public func add(child: XmlElement) -> XmlElement? {
        xmlUnlinkNode(child.nodePointer)
        if let newPointer = xmlAddChild(nodePointer, child.nodePointer) {
            if newPointer == child.nodePointer {
                return child
            } else if newPointer == nodePointer {
                return self
            } else {
                return XmlElement(node: newPointer)
            }
        } else {
            return nil
        }
    }

    @discardableResult
    public func add(next sibling: XmlElement) -> XmlElement? {
        if let newPointer = xmlAddNextSibling(nodePointer, sibling.nodePointer) {
            return newPointer == nodePointer ? self : sibling
        } else {
            return nil
        }
    }

    @discardableResult
    public func add(previous sibling: XmlElement) -> XmlElement? {
        if let newPointer = xmlAddPrevSibling(nodePointer, sibling.nodePointer) {
            return newPointer == nodePointer ? self : sibling
        } else {
            return nil
        }
    }

    @discardableResult
    public func add(sibling: XmlElement) -> XmlElement? {
        if let newPointer = xmlAddSibling(nodePointer, sibling.nodePointer) {
            return newPointer == nodePointer ? self : sibling
        } else {
            return nil
        }
    }

    public func delete() {
        parent?.remove(child: self)
    }

    @discardableResult
    public func replace(with newNode: XmlElement) -> XmlElement? {
        if let old = xmlReplaceNode(/* old */nodePointer, /* cur */newNode.nodePointer) {
            xmlFreeNode(old)
            nodePointer = newNode.nodePointer
            return self
        } else {
            return nil
        }
    }

    public func copy(recursive: Bool) -> XmlElement? {
        if let copy = xmlCopyNode(nodePointer, recursive ? 1 : 2) {
            return XmlElement(node: copy)
        } else {
            return nil
        }
    }

    public var content: String? {
        get {
            switch type {
            case .textNode:
                return String(cString: nodePointer.pointee.content)
            default: return nil
            }
        }
        set {
            let escaped = xmlEncodeEntitiesReentrant(nodePointer.pointee.doc, newValue)
            defer { xmlFree(escaped) }
            xmlNodeSetContent(nodePointer, escaped)
        }
    }

    public var ancestors: AnyIterator<XmlElement> {
        AnyIterator(XmlAncestorIterator(start: self))
    }

    public var descendants: AnyIterator<XmlElement> {
        AnyIterator(XmlDescendantsIterator(start: self))
    }

    public var siblings: AnyIterator<XmlElement> {
        AnyIterator<XmlElement>(XmlSiblingIterator(start: self))
    }

    public var description: String {
        switch type {
        case .elementNode, .attributeNode:
            return String(cString: nodePointer.pointee.name)
        case .textNode:
            return content!
        default:
            return type?.description ?? "null"
        }
    }

    public var debugDescription: String {
        "[\(type?.description ?? "unknown")]: \(description)"
    }

    public static func == (lhs: XmlElement, rhs: XmlElement) -> Bool {
        lhs.nodePointer == rhs.nodePointer
    }

    public func hash(into hasher: inout Hasher) {
        hasher.combine(nodePointer)
    }
}
