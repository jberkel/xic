// Enum xmlElementType
public enum XmlElementType: UInt32, CaseIterable, CustomStringConvertible, Equatable {
    case elementNode = 1
    case attributeNode = 2
    case textNode = 3
    case cdataSectionNode = 4
    case entityRefNode = 5
    case entityNode = 6
    case piNode = 7
    case commentNode = 8
    case documentNode = 9
    case documentTypeNode = 10
    case documentFragNode = 11
    case notationNode = 12
    case htmlDocumentNode = 13
    case dtdNode = 14
    case elementDecl = 15
    case attributeDecl = 16
    case entityDecl = 17
    case namespaceDecl = 18
    case xincludeStart = 19
    case xincludeEnd = 20
    case docbDocumentNode = 21

    public var description: String {
        switch self {
        case .elementNode:
            return "element"
        case .attributeNode:
            return "attribute"
        case .textNode:
            return "text"
        case .cdataSectionNode:
            return "cdata_selection_node"
        case .entityRefNode:
            return "entity_ref_node"
        case .entityNode:
            return "entity_node"
        case .piNode:
            return "pi_node"
        case .commentNode:
            return "comment_node"
        case .documentNode:
            return "document_node"
        case .documentTypeNode:
            return "document_type_node"
        case .documentFragNode:
            return "document_frag_node"
        case .notationNode:
            return "notation_node"
        case .htmlDocumentNode:
            return "html_document_node"
        case .dtdNode:
            return "dtd_node"
        case .elementDecl:
            return "element_decl"
        case .attributeDecl:
            return "attribute_decl"
        case .entityDecl:
            return "entity_decl"
        case .namespaceDecl:
            return "namespace_decl"
        case .xincludeStart:
            return "xinclude_start"
        case .xincludeEnd:
            return "xinclude_end"
        case .docbDocumentNode:
            return "docb_document_node"
        }
    }
}
