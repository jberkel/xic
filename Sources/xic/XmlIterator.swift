import Foundation
import Clibxml2

struct XmlDescendantsIterator: IteratorProtocol {
    let iterator: AnyIterator<XmlElement>

    init(start: XmlElement) {
        self.iterator = start.children.map(XmlDescendantsAndSiblingsIterator.init).map(AnyIterator.init) ??
            AnyIterator<XmlElement> { nil }
    }

    func next() -> XmlElement? {
        iterator.next()
    }
}

struct XmlDescendantsAndSiblingsIterator: IteratorProtocol {
    private var stack: [XmlElement]

    init(start: XmlElement) {
        stack = [start]
    }

    mutating func next() -> XmlElement? {
        guard let current = stack.popLast() else { return nil }
        if let next = current.next {
            stack.append(next)
        }
        if let children = current.children {
            stack.append(children)
        }
        return current
    }
}

struct XmlAncestorIterator: IteratorProtocol {
    private var current: XmlElement?

    init(start: XmlElement) {
        current = start.parent
    }

    mutating func next() -> XmlElement? {
        guard let next = current, next.type != .documentNode else { return nil }
        current = next.parent
        return next
    }
}

struct XmlSiblingIterator: IteratorProtocol {
    private var current: XmlElement?

    init(start: XmlElement) {
        current = start
    }

    mutating func next() -> XmlElement? {
        current = current?.next
        return current
    }
}
