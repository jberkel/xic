import XCTest
@testable import xic

class StringExtensionTests: XCTestCase {
    func testTrim() {
        XCTAssertEqual(" das \n ".trim, "das")
    }

    func testTrimEmpty() {
        XCTAssertEqual("".trim, "")
    }
}
