import XCTest
@testable import xic

class XPathTests: XCTestCase {
    func testXPathQuery() throws {
        let doc = try Xic(string: "<div><span>test</span></div>")
        let result = doc.xpath("//span", contextNode: doc.root)

        guard case .nodeSet(let nodes) = result else { fatalError("Unexpected result: \(String(describing: result))") }

        XCTAssertEqual(nodes.count, 1)
        XCTAssertEqual(nodes.first?.description, "span")
    }

    func testXPathQueryTextNodes() throws {
        let doc = try Xic(string: "<div><span>test</span><p>foo</p></div>")
        let result = doc.xpath("//text()", contextNode: doc.root)

        guard case .nodeSet(let nodes) = result else { fatalError("Unexpected result: \(String(describing: result))") }

        XCTAssertEqual(nodes.count, 2)
        XCTAssertEqual(nodes.description, "testfoo")
    }

    func testXPathQueryStringQuery() throws {
        let doc = try Xic(string: "<div><span>test</span><p>foo</p></div>")
        // If the value of the argument is a node-set, the function returns the string-value of the first node
        // in document order, ignoring any further
        let result = doc.xpath("string(//text())", contextNode: doc.root)
        guard case .string(let string) = result else { fatalError("Unexpected result: \(String(describing: result))") }

        XCTAssertEqual(string, "test")
    }

    func testXPathWithoutMatchingNamespacesReturnsNil() throws {
        let doc = try Xic(string: """
                                  <div xmlns="http://www.w3.org/1999/xhtml"><span>test</span><p>foo</p></div>
                                  """)

        XCTAssertEqual(doc.description, """
                                        <div xmlns="http://www.w3.org/1999/xhtml"><span>test</span><p>foo</p></div>
                                        """)

        let result = doc.xpath("//span")
        XCTAssertNil(result)
    }

    func testXPathWithMatchingNamespacesReturnsResults() throws {
        let doc = try Xic(string: """
                                  <div xmlns="http://www.w3.org/1999/xhtml"><span>test</span><p>foo</p></div>
                                  """)
        let result = doc.xpath("string(//xhtml:span)", namespaces: Xic.XHTML_NS)
        guard case .string(let string) = result else { fatalError("Unexpected result: \(String(describing: result))") }

        XCTAssertEqual(string, "test")
    }
}
