import XCTest
@testable import xic

class XicTests: XCTestCase {

    func testRoot() throws {
        let subject = try Xic(string: "<div><span>test</span></div>")
        XCTAssertEqual(subject.root.description, "div")
    }

    func testDescription() throws {
        let subject = try Xic(string: "<div><span>test</span></div>")
        XCTAssertEqual(subject.description, "<div><span>test</span></div>")
    }

    func testPrettyPrint() throws {
        let subject = try Xic(string: "<div><span><baz>123</baz></span></div>")
        XCTAssertEqual(subject.prettyPrint,
           """
           <div>
             <span>
               <baz>123</baz>
             </span>
           </div>
           """)
    }

    func testPrettyPrintPreservingWhitespace() throws {
        let subject = try Xic(string: "<div><span><baz>123</baz></span></div>")
        XCTAssertEqual(subject.prettyPrint,
           """
           <div>
             <span>
               <baz>123</baz>
             </span>
           </div>
           """)
    }

    func testDebugDescription() throws {
        let subject = try Xic(string: "<div><span>test</span></div>")
        XCTAssertEqual(subject.debugDescription, """
                                                 [element]: div
                                                 [element]: span
                                                 [text]: test
                                                 """)
    }

    func testParserThrowsInvalidXml() {
        XCTAssertThrowsError(try Xic(string: "malformed")) { error in
            if case ParserError.invalidXml(let line, let message) = error {
                XCTAssertEqual(message, "Start tag expected, '<' not found")
                XCTAssertEqual(line, 1)
            } else {
                fatalError("unexpected error: \(error)")
            }
        }
    }

    func testParseAcceptsParseOptions() {
        let xic = try! Xic(string: "<div>\n\n     <p>dsadas</p>\n  <p>foo</p></div>", options: [.noError, .noBlanks])
        XCTAssertEqual(xic.prettyPrint,
           """
           <div>
             <p>dsadas</p>
             <p>foo</p>
           </div>
           """)
    }
}
