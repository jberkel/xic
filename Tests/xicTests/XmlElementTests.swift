import XCTest
@testable import xic

class XmlElementTests: XCTestCase {
    func testStringInitializer() {
        let element = XmlElement(name: "test")
        XCTAssertEqual(element.description, "test")
    }

    func testGetContent() throws {
        let doc = try Xic(string: "<p>baz</p>")
        XCTAssertEqual(doc.root.children?.content, "baz")
    }

    func testSetContent() throws {
        let doc = try Xic(string: "<p>baz</p>")
        doc.root.content = "bar"
        XCTAssertEqual(doc.description, "<p>bar</p>")
    }

    func testSetContentAutomaticallyEscapesEntities() throws {
        let doc = try Xic(string: "<p>baz</p>")
        doc.root.content = "<bar>"
        XCTAssertEqual(doc.description, "<p>&lt;bar&gt;</p>")
    }

    func testElementNodeInitializerWithAttributes() {
        let element = XmlElement(name: "test", attributes: ["class": "test", "alt": "bar"])
        XCTAssertEqual(Xic(root: element).description, "<test alt=\"bar\" class=\"test\"/>")
    }

    func testTextNodeInitializer() {
        let element = XmlElement(text: "this is a text node")
        XCTAssertEqual(element.type, .textNode)
        XCTAssertEqual(Xic(root: element).description, "this is a text node")
    }

    func testGetAttribute() throws {
        let doc = try Xic(string: "<div class=\"bar\"></div>")
        XCTAssertEqual(doc.root["class"], "bar")
    }

    func testSetAttribute() throws {
        let doc = try Xic(string: "<div class=\"bar\"></div>")
        doc.root["class"] = "baz"
        XCTAssertEqual(doc.description, "<div class=\"baz\"/>")
    }

    func testAddChild() {
        let div = XmlElement(name: "div")
        let p = XmlElement(name: "p")
        let child = div.add(child: p)
        XCTAssertTrue(child === p)
        XCTAssertEqual(child?.parent?.nodePointer, div.nodePointer)
        XCTAssertEqual(Xic(root: div).description, "<div><p/></div>")
    }

    func testAddChildTextNodeMerge() {
        let text1 = XmlElement(text: "t1")
        let text2 = XmlElement(text: "t2")
        let child = text1.add(child: text2)
        XCTAssertTrue(child === text1)
    }

    func testAddSibling() {
        let text = XmlElement(text: "Some other text")
        let root = XmlElement(name: "root")
        root.add(child: text)

        let tag = XmlElement(name: "tag")
        tag.add(child: XmlElement(text: "tag-content"))

        text.add(sibling: tag)
        text.add(sibling: XmlElement(text: "more text"))

        XCTAssertEqual(Xic(root: root).description, "<root>Some other text<tag>tag-content</tag>more text</root>")
    }

    func testAddNextSibling() {
        let root = XmlElement(name: "root")
        let child = root.add(child: XmlElement(name: "child"))

        let grandchild1 = child?.add(next: XmlElement(name: "grandchild1"))
        grandchild1?.add(next: XmlElement(name: "grandchild2"))

        XCTAssertEqual(Xic(root: root).description, "<root><child/><grandchild1/><grandchild2/></root>")
    }

    func testAddNextTextNodeMerge() {
        let text1 = XmlElement(text: "foo")
        let text2 = XmlElement(text: "bar")
        XCTAssertTrue(text1.add(next: text2) === text1)
    }

    func testAddPreviousTextNodeMerge() {
        let text1 = XmlElement(text: "foo")
        let text2 = XmlElement(text: "bar")
        XCTAssertTrue(text2.add(previous: text1) === text2)
    }

    func testAddSiblingTextNodeMerge() {
        let text1 = XmlElement(text: "foo")
        let text2 = XmlElement(text: "bar")
        XCTAssertTrue(text1.add(sibling: text2) === text1)
    }

    func testAddPreviousSibling() {
        let root = XmlElement(name: "root")
        let child = root.add(child: XmlElement(name: "child"))

        let grandchild1 = child?.add(next: XmlElement(name: "grandchild1"))
        let grandchild2 = grandchild1?.add(next: XmlElement(name: "grandchild2"))
        grandchild2?.add(previous: XmlElement(name: "grandchild3"))

        XCTAssertEqual(Xic(root: root).description, "<root><child/><grandchild1/><grandchild3/><grandchild2/></root>")
    }

    func testReplaceNode() throws {
        let doc = try Xic(string: "<div><p>baz</p></div>")
        let p = doc.first("p")!
        let a = XmlElement(name: "a")

        p.replace(with: a)

        XCTAssertEqual(doc.description, "<div><a/></div>")
    }

    func testDeleteNode() throws {
        let doc = try Xic(string: "<div><p>baz</p></div>")
        let p = doc.first("p")!
        p.delete()
        XCTAssertEqual(doc.description, "<div/>")
    }

    func testCopyNodeRecursively() throws {
        let doc = try Xic(string: #"<div><p class="test"><em>baz</em></p></div>"#)
        let p = doc.first("p")!
        let copy = p.copy(recursive: true)!
        XCTAssertEqual(Xic(root: copy).description, #"<p class="test"><em>baz</em></p>"#)
    }

    func testCopyNodeSimple() throws {
        let doc = try Xic(string: #"<div><p class="test"><em>baz</em></p></div>"#)
        let p = doc.first("p")!
        let copy = p.copy(recursive: false)!
        XCTAssertEqual(Xic(root: copy).description, #"<p class="test"/>"#)
    }

    func testConstructTree() {
        let root = XmlElement(name: "root")

        root.add(child: XmlElement(name: "child"))?
            .add(child: XmlElement(text: "some text"))?
            .add(sibling: XmlElement(text: " and some text"))

        XCTAssertEqual(Xic(root: root).prettyPrint, """
                                                    <root>
                                                      <child>some text and some text</child>
                                                    </root>
                                                    """)
    }

    func testAncestors() throws {
        let doc = try Xic(string: "<html><body><a><em>text</em></a></body></html>")
        let em = doc.first("//em")!

        XCTAssertEqual(Array(em.ancestors).map(\.description), ["a", "body", "html"])
        XCTAssertNil(doc.root.ancestors.next())
    }

    func testSiblings() throws {
        let doc = try Xic(string: "<root><a/><b>b_content</b><c>c_cont</c><d/></root>")
        let element = doc.first("//b")!

        XCTAssertEqual(Array(element.siblings).map(\.description), ["c", "d"])
        XCTAssertNil(doc.root.ancestors.next())
    }

    func testEqual() throws {
        let doc = try Xic(string: "<html><body><a><em>text</em></a></body></html>")

        XCTAssertEqual(doc.first("//html"), doc.root)
        XCTAssertNotEqual(doc.first("//em"), doc.root)
    }

    func testHashable() throws {
        let doc = try Xic(string: "<html><body><a><em>text</em></a></body></html>")

        XCTAssertEqual(doc.root.hashValue, doc.root.hashValue)
        XCTAssertEqual(doc.first("//html")!.hashValue, doc.root.hashValue)
        XCTAssertNotEqual(doc.first("//em").hashValue, doc.root.hashValue)
    }
}
