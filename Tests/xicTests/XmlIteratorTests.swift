import XCTest
@testable import xic

class XmlIteratorTests: XCTestCase {
    func testDocumentIteratorReturnsAllElements() throws {
        let document = try Xic(string: """
                                       <root><a><b>b_content</b><c>c_content</c></a></root>
                                       """)
        let nodes = Array(document)
        XCTAssertEqual(nodes.map(\.description), ["root", "a", "b", "b_content", "c", "c_content"])
    }

    func testElementIteratorReturnsDescendantsOfElement() throws {
        let document = try Xic(string: """
                                       <root><a><b>b_content</b><c>c_content</c></a></root>
                                       """)
        let element = document.first("//a")!
        let children = Array(element)
        XCTAssertEqual(children.map(\.description), ["b", "b_content", "c", "c_content"])
    }
}
