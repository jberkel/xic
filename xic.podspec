Pod::Spec.new do |s|
  s.name      = 'xic'
  s.version   = '0.0.1'
  s.summary   = 'Simple libxml2 bindings for Swift inspired by Kanna.'
  s.homepage  = 'https://gitlab.com/jberkel/xic'
  s.license   = 'MIT'
  s.author    = { 'Jan Berkel' => 'jan@berkel.fr' }
  s.source    = { :git => 'https://gitlab.com/jberkel/xic.git', :tag =>s.version.to_s }

  s.swift_version = '5.1'
  s.ios.deployment_target = '11.0'
  s.osx.deployment_target = '10.13'

  s.preserve_path = 'Sources/Clibxml2/*'
  s.source_files  = ['Sources/**/*.swift']
  s.xcconfig      = {
    'HEADER_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2',
    'SWIFT_INCLUDE_PATHS' => '$(PODS_TARGET_SRCROOT)/Sources/Clibxml2'
  }

  s.test_spec do |test_spec|
    test_spec.source_files  = ['Tests/**/*.swift']
  end
end
